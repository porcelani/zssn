package com.zssn.core.service;

import com.zssn.core.domain.Item;
import com.zssn.core.domain.Survivor;
import com.zssn.core.domain.Trade;
import com.zssn.infraestruture.InMemorySurvivorRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.zssn.core.domain.Item.*;
import static org.junit.Assert.assertEquals;

public class TradeServiceTest {

  private static final String SURVIVOR_A = "survivor_a";
  private static final String SURVIVOR_B = "survivor_b";
  private TradeService tradeService;
  private SurvivorService survivorService;

  @Before
  public void setUp() {
    survivorService = new SurvivorService(new InMemorySurvivorRepository());
    tradeService = new TradeService(survivorService);
  }

  @Test
  public void shouldTradeResources() throws Exception {
    //create Survivors
    Survivor survivorA = new Survivor();
    survivorA.setName(SURVIVOR_A);
    HashMap<Item, Integer> resourcesA = new HashMap<>();
    resourcesA.put(WATER, 1);
    resourcesA.put(MEDICATION, 1);
    survivorA.setResources(resourcesA);
    survivorService.create(survivorA);

    Survivor survivorB = new Survivor();
    survivorB.setName(SURVIVOR_B);
    HashMap<Item, Integer> resourcesB = new HashMap<>();
    resourcesB.put(AMMUNITION, 6);
    survivorB.setResources(resourcesB);
    survivorService.create(survivorB);

    //create trade
    Survivor survivorAToTrade = new Survivor();
    survivorAToTrade.setName(SURVIVOR_A);
    HashMap<Item, Integer> tradeResourcesA = new HashMap<>();
    tradeResourcesA.put(WATER, 1);
    survivorAToTrade.setResources(tradeResourcesA);

    Survivor survivorBToTrade = new Survivor();
    survivorBToTrade.setName(SURVIVOR_B);
    HashMap<Item, Integer> tradeResourcesB = new HashMap<>();
    tradeResourcesB.put(AMMUNITION, 4);
    survivorBToTrade.setResources(tradeResourcesB);
    Trade trade = new Trade(survivorAToTrade, survivorBToTrade);


    tradeService.tradeResources(trade);


    Survivor survivorASaved = survivorService.findThrowNotFound(SURVIVOR_A);
    Survivor survivorBSaved = survivorService.findThrowNotFound(SURVIVOR_B);
    assertEquals(survivorASaved.resourcesPoints(), survivorBSaved.resourcesPoints());

    Map<Item, Integer> resourceASaved = survivorASaved.getResources();
    assertEquals(0, resourceASaved.get(WATER).intValue());
    assertEquals(0, resourceASaved.get(FOOD).intValue());
    assertEquals(1, resourceASaved.get(MEDICATION).intValue());
    assertEquals(4, resourceASaved.get(AMMUNITION).intValue());

    Map<Item, Integer> resourceBSaved = survivorBSaved.getResources();
    assertEquals(1, resourceBSaved.get(WATER).intValue());
    assertEquals(0, resourceBSaved.get(FOOD).intValue());
    assertEquals(0, resourceBSaved.get(MEDICATION).intValue());
    assertEquals(2, resourceBSaved.get(AMMUNITION).intValue());

  }

}
