package com.zssn.core.service;

import com.zssn.core.domain.Item;
import com.zssn.core.domain.Reports;
import com.zssn.core.domain.Survivor;
import com.zssn.infraestruture.InMemorySurvivorRepository;
import com.zssn.utils.SurvivorEnviroment;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static com.zssn.core.domain.Item.*;
import static java.lang.Float.valueOf;
import static org.junit.Assert.assertEquals;

public class ReportServiceTest {

  private SurvivorService survivorService;
  private ReportService reportService;

  @Before
  public void setUp() {
    survivorService = new SurvivorService(new InMemorySurvivorRepository());
    reportService = new ReportService(survivorService);
  }

  @Test
  public void shouldShowReports() {
    Survivor survivorA = SurvivorEnviroment.createSimpleSurvivor();
    survivorService.create(survivorA);
    Survivor survivorB = SurvivorEnviroment.createOtherSurvivor();
    survivorService.create(survivorB);


    Reports reports = reportService.getReports();


    assertEquals(valueOf(0), reports.getPercentageOfInfectedSurvivors());
    assertEquals(valueOf(100), reports.getPercentageOfNonInfectedSurvivors());
    Map<Item, Integer> averageOfResources = reports.getAverageOfResources();
    assertEquals(0, averageOfResources.get(WATER).intValue());
    assertEquals(1, averageOfResources.get(FOOD).intValue());
    assertEquals(0, averageOfResources.get(MEDICATION).intValue());
    assertEquals(4, averageOfResources.get(AMMUNITION).intValue());
    assertEquals(0, reports.getPointsOfResourcesLost().intValue());
  }
}
