package com.zssn.utils;

import com.zssn.core.domain.Item;
import com.zssn.core.domain.Location;
import com.zssn.core.domain.Survivor;

import java.util.HashMap;
import java.util.Map;

import static com.zssn.core.domain.Gender.FEMALE;
import static com.zssn.core.domain.Gender.MALE;
import static com.zssn.core.domain.Item.*;

public class SurvivorEnviroment {

  public static Survivor createSimpleSurvivor() {
    return createSimpleSurvivor("Porcelani");
  }

  public static Survivor createOtherSurvivor() {
    Survivor survivor = createSimpleSurvivor("Silva");
    survivor.setGender(FEMALE);
    Map<Item, Integer> resources = new HashMap<>();
    resources.put(MEDICATION, 1);
    resources.put(AMMUNITION, 4);
    survivor.setResources(resources);
    return survivor;
  }

  public static Survivor createSimpleSurvivor(String survivorName) {
    Survivor survivor = new Survivor();
    survivor.setName(survivorName);
    survivor.setAge(32);
    survivor.setGender(MALE);
    survivor.setLocation(new Location(-23.4059175, -52.0380783));
    Map<Item, Integer> resources = new HashMap<>();
    resources.put(WATER, 1);
    resources.put(FOOD, 2);
    resources.put(AMMUNITION, 4);
    survivor.setResources(resources);
    return survivor;
  }
}
