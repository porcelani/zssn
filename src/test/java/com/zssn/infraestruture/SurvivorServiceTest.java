package com.zssn.infraestruture;

import com.zssn.core.domain.Location;
import com.zssn.core.domain.Survivor;
import com.zssn.core.service.SurvivorService;
import org.junit.Before;
import org.junit.Test;

import java.util.TreeSet;

import static com.zssn.utils.SurvivorEnviroment.createSimpleSurvivor;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

public class SurvivorServiceTest {

  private SurvivorService survivorService;

  @Before
  public void setUp() {
    survivorService = new SurvivorService(new InMemorySurvivorRepository());
  }

  @Test
  public void shouldCreateTheSurvivor() {
    Survivor simpleSurvivor = createSimpleSurvivor();


    Survivor savedSurvivor = survivorService.create(simpleSurvivor);


    assertSurvivorInformation(savedSurvivor);
  }

  @Test
  public void shouldUpdateSurvivorLocation() {
    Survivor savedSurvivor = survivorService.create(createSimpleSurvivor());
    Location maringaLocation = savedSurvivor.getLocation();


    Location saoPauloLocation = new Location(-22.5305866, -50.8792296);
    savedSurvivor.setLocation(saoPauloLocation);
    survivorService.updateLocation(savedSurvivor);


    assertNotEquals(maringaLocation, survivorService.find(savedSurvivor.getName()).get().getLocation());
    assertEquals(saoPauloLocation, survivorService.find(savedSurvivor.getName()).get().getLocation());
  }

  @Test
  public void shouldAddReportThatSurvivorWasInfectedButNotConfirmed() {
    Survivor savedSurvivor = survivorService.create(createSimpleSurvivor());

    TreeSet<String> survivorsNames = new TreeSet<>();
    survivorsNames.add("silva");
    savedSurvivor.flagTheSurvivorAsInfected(survivorsNames);


    survivorService.addReportThatSurvivorWasInfected(savedSurvivor);


    Survivor survivor = survivorService.find(savedSurvivor.getName()).get();
    assertFalse(survivor.getInfected());
    assertEquals(1, survivor.whoFlagTheSurvivorAsInfected().size());
  }

  @Test
  public void shouldAddReportThatSurvivorWasInfectedButConfirmed() {
    Survivor savedSurvivor = survivorService.create(createSimpleSurvivor());

    TreeSet<String> survivorsNames = new TreeSet<>();
    survivorsNames.add("silva");
    survivorsNames.add("torres");
    survivorsNames.add("santos");
    savedSurvivor.flagTheSurvivorAsInfected(survivorsNames);


    survivorService.addReportThatSurvivorWasInfected(savedSurvivor);


    assertFalse(survivorService.find("porcelani").isPresent());
  }



  private void assertSurvivorInformation(Survivor savedSurvivor) {
    assertThat(savedSurvivor.getName(), equalTo("porcelani"));
    assertThat(savedSurvivor.getAge(), equalTo(32));
    assertFalse(savedSurvivor.isInfected());
    assertThat(savedSurvivor.resourcesPoints(), equalTo(14));
  }
}
