package com.zssn.controller;

import com.zssn.core.domain.Item;
import com.zssn.core.domain.Survivor;
import com.zssn.core.domain.Trade;
import com.zssn.utils.SurvivorEnviroment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.zssn.core.domain.Item.AMMUNITION;
import static com.zssn.core.domain.Item.WATER;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TradeControllerTest {

  private static final String SURVIVOR_A = "survivor_a";
  private static final String SURVIVOR_B = "survivor_b";

  @LocalServerPort
  private int port;

  private URL base;

  @Autowired
  private TestRestTemplate template;

  @Before
  public void setUp() throws Exception {
    this.base = new URL("http://localhost:" + port);
  }

  @Test
  public void shouldAddSurvivorsToTheDatabase() {
    createSurvivor(SURVIVOR_A);
    createSurvivor(SURVIVOR_B);

    //create trade
    Survivor survivorAToTrade = new Survivor();
    survivorAToTrade.setName(SURVIVOR_A);
    Map<Item, Integer> tradeResourcesA = new HashMap<>();
    tradeResourcesA.put(WATER, 1);
    survivorAToTrade.setResources(tradeResourcesA);

    Survivor survivorBToTrade = new Survivor();
    survivorBToTrade.setName(SURVIVOR_B);
    Map<Item, Integer> tradeResourcesB = new HashMap<>();
    tradeResourcesB.put(AMMUNITION, 4);
    survivorBToTrade.setResources(tradeResourcesB);
    Trade trade = new Trade(survivorAToTrade, survivorBToTrade);


    ResponseEntity tradeResponseEntity = template.postForEntity(base.toString() + "/trades", trade, Trade.class);


    assertEquals(HttpStatus.CREATED, tradeResponseEntity.getStatusCode());
  }



  private Survivor createSurvivor(String name) {
    Survivor simpleSurvivor = SurvivorEnviroment.createSimpleSurvivor(name);
    simpleSurvivor.setName(name);

    return template.postForEntity(base.toString() + "/survivors", simpleSurvivor, Survivor.class).getBody();
  }


}
