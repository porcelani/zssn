package com.zssn.controller;

import com.zssn.core.domain.Location;
import com.zssn.core.domain.Survivor;
import com.zssn.utils.SurvivorEnviroment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;
import java.util.List;

import static com.zssn.core.domain.Gender.MALE;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SurvivorControllerTest {

  @LocalServerPort
  private int port;

  private URL base;

  @Autowired
  private TestRestTemplate template;

  @Before
  public void setUp() throws Exception {
    this.base = new URL("http://localhost:" + port + "/survivors");
  }

  @Test
  public void shouldAddSurvivorsToTheDatabase() {
    ResponseEntity<Survivor> response = createSurvivor();


    assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));

    Survivor survivorSaved = response.getBody();
    assertThat(survivorSaved.getName(), equalTo("porcelani"));
    assertThat(survivorSaved.getAge(), equalTo(32));
    assertThat(survivorSaved.getGender(), equalTo(MALE));

    Location location = survivorSaved.getLocation();
    assertThat(location.getLatitude(), equalTo(-23.4059175));
    assertThat(location.getLongitude(), equalTo(-52.0380783));

    assertThat(survivorSaved.resourcesPoints(), equalTo(14));

    assertFalse(survivorSaved.isInfected());
    assertTrue(survivorSaved.whoFlagTheSurvivorAsInfected().isEmpty());
  }

  @Test
  public void shouldListSurvivors() {
    createSurvivor();


    ResponseEntity<List> response = template.getForEntity(base.toString(), List.class);


    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    List<Survivor> body = response.getBody();
    assertThat(body.size(), equalTo(1));
  }



  private ResponseEntity<Survivor> createSurvivor() {
    Survivor simpleSurvivor = SurvivorEnviroment.createSimpleSurvivor();

    return template.postForEntity(base.toString(), simpleSurvivor, Survivor.class);
  }


}
