package com.zssn.infraestruture;

import com.zssn.core.exception.SurvivorNotFoundException;
import com.zssn.core.domain.Survivor;
import com.zssn.core.ports.SurvivorRepository;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Repository
public class InMemorySurvivorRepository implements SurvivorRepository {
    private final ConcurrentMap<String, Survivor> inMemoryDb;

    public InMemorySurvivorRepository() {
        this.inMemoryDb = new ConcurrentHashMap<>();
    }

    @Override
    public Survivor create(Survivor survivor) {
        if (null != inMemoryDb.putIfAbsent(survivor.getName(), survivor)) {
            return null;
        }
        return survivor;
    }

    @Override
    public Optional<Survivor> findByName(final String name) {
        return Optional.ofNullable(inMemoryDb.get(name));
    }

    @Override
    public Survivor findThrowNotFound(String name) throws SurvivorNotFoundException {
        Survivor survivor = inMemoryDb.get(name);
        if (null == survivor) {
            throw new SurvivorNotFoundException();
        }
        return survivor;
    }

    @Override
    public List<Survivor> listSurvivors() {
        return inMemoryDb.values()
                .stream()
                .sorted(Comparator.comparing(Survivor::getName))
                .collect(Collectors.toList());
    }

    @Override
    public Survivor save(Survivor survivor) {
        if (null == inMemoryDb.replace(survivor.getName(), survivor)) {
            return null;
        }
        return survivor;
    }

}
