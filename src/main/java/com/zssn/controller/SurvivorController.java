package com.zssn.controller;

import com.zssn.core.domain.Survivor;
import com.zssn.core.exception.SurvivorNotFoundException;
import com.zssn.core.service.SurvivorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/survivors")
public class SurvivorController {

    @Autowired
    private SurvivorService survivorService;

    @GetMapping
    @ResponseBody
    public List<Survivor> listSurvivors() {
        return survivorService.listSurvivors();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Survivor registerSurvivor(@RequestBody Survivor survivor) {
        return survivorService.create(survivor);
    }

    @GetMapping(path = "/{name}")
    @ResponseBody
    public Survivor listSurvivor(@PathVariable(value = "name") String name) {
        return survivorService.find(name).orElseThrow(SurvivorNotFoundException::new);
    }

    @PutMapping(path = "/{name}")
    @ResponseBody
    public void updateSurvivorLocation(@PathVariable(value = "name") String name, @RequestBody Survivor survivor) {
        survivor.setName(name);
        survivorService.updateLocation(survivor);
    }

    @PostMapping(path = "/{name}")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void flagSurvivorAsInfected(@PathVariable(value = "name") String name, @RequestBody Survivor survivor) {
        survivor.setName(name);
        survivorService.addReportThatSurvivorWasInfected(survivor);
    }

}
