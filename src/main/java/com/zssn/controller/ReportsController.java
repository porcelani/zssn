package com.zssn.controller;

import com.zssn.core.domain.Reports;
import com.zssn.core.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/reports")
public class ReportsController {

  @Autowired
  private ReportService reportService;


  @GetMapping
  @ResponseBody
  public Reports getReports() {
    return reportService.getReports();
  }

}
