package com.zssn.controller;

import com.zssn.core.domain.Trade;
import com.zssn.core.service.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/trades")
public class TradeController {

  @Autowired
  private TradeService tradeService;


  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public void tradeItems(@RequestBody Trade trade) throws Exception {
    tradeService.tradeResources(trade);
  }

}
