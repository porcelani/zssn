package com.zssn.core.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Reports {
  @JsonProperty("percentage_of_infected_survivors")
  private Float percentageOfInfectedSurvivors;

  @JsonProperty("percentage_of_non_infected_survivors")
  private Float percentageOfNonInfectedSurvivors;

  @JsonProperty("average_of_resources")
  private Map<Item, Integer> averageOfResources;

  @JsonProperty("points_of_resources_lost")
  private Integer pointsOfResourcesLost;


  public Reports() {
  }

  public Float getPercentageOfInfectedSurvivors() {
    return percentageOfInfectedSurvivors;
  }

  public void setPercentageOfInfectedSurvivors(Float percentageOfInfectedSurvivors) {
    this.percentageOfInfectedSurvivors = percentageOfInfectedSurvivors;
  }

  public Float getPercentageOfNonInfectedSurvivors() {
    return percentageOfNonInfectedSurvivors;
  }

  public void setPercentageOfNonInfectedSurvivors(Float percentageOfNonInfectedSurvivors) {
    this.percentageOfNonInfectedSurvivors = percentageOfNonInfectedSurvivors;
  }

  public Map<Item, Integer> getAverageOfResources() {
    return averageOfResources;
  }

  public void setAverageOfResources(Map<Item, Integer> averageOfResources) {
    this.averageOfResources = averageOfResources;
  }

  public Integer getPointsOfResourcesLost() {
    return pointsOfResourcesLost;
  }

  public void setPointsOfResourcesLost(Integer pointsOfResourcesLost) {
    this.pointsOfResourcesLost = pointsOfResourcesLost;
  }
}
