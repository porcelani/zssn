package com.zssn.core.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Survivor {
    private String name;
    private Integer age;
    private Gender gender;
    private Boolean infected;
    private Location location;
    private Map<Item, Integer> resources = new HashMap<>();

    @JsonProperty("who_flag_the_survivor_as_infected")
    private Set<String> whoFlagTheSurvivorAsInfected = new TreeSet<>();


    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    public boolean isInfected() {
        return infected;
    }

    public Location getLocation() {
        return location;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setInfected(boolean infected) {
        this.infected = infected;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Boolean getInfected() {
        return infected;
    }

    public void setInfected(Boolean infected) {
        this.infected = infected;
    }

    public Set<String> whoFlagTheSurvivorAsInfected() {
        return whoFlagTheSurvivorAsInfected;
    }

    public void flagTheSurvivorAsInfected(Set<String> survivorsNames) {
        this.whoFlagTheSurvivorAsInfected.addAll(survivorsNames);
    }

    public Map<Item, Integer> getResources() {
        Arrays.asList(Item.values()).stream().forEach(item -> {
            resources.putIfAbsent(item, 0);
        });
        return resources;
    }

    public void setResources(Map<Item, Integer> resources) {
        this.resources = resources;
    }

    public Integer resourcesPoints() {
        Set<Item> items = resources.keySet();
        return items.stream()
                .mapToInt(item -> item.points * resources.get(item))
                .sum();
    }
}
