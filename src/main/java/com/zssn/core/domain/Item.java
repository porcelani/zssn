package com.zssn.core.domain;

public enum Item {
  WATER(4),
  FOOD(3),
  MEDICATION(2),
  AMMUNITION(1);

  public final Integer points;

  Item(Integer points) {
    this.points = points;
  }
}
