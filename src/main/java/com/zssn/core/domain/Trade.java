package com.zssn.core.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zssn.core.exception.TradeUnrespectPriceTableException;

public class Trade {

  @JsonProperty("survivor_a")
  private Survivor survivorA;

  @JsonProperty("survivor_b")
  private Survivor survivorB;

  public Trade() {
  }

  public Trade(Survivor survivorA, Survivor survivorB) {
    if (!survivorA.resourcesPoints().equals(survivorB.resourcesPoints())) {
      throw new TradeUnrespectPriceTableException();
    }
    this.survivorA = survivorA;
    this.survivorB = survivorB;
  }

  public Survivor getSurvivorA() {
    return survivorA;
  }

  public void setSurvivorA(Survivor survivorA) {
    this.survivorA = survivorA;
  }

  public Survivor getSurvivorB() {
    return survivorB;
  }

  public void setSurvivorB(Survivor survivorB) {
    this.survivorB = survivorB;
  }

}
