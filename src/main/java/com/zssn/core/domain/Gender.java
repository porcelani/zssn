package com.zssn.core.domain;

public enum Gender {
  MALE, FEMALE
}
