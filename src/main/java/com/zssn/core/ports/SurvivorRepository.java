package com.zssn.core.ports;

import com.zssn.core.domain.Survivor;

import java.util.List;
import java.util.Optional;

public interface SurvivorRepository {
  Survivor create(Survivor survivor);

  Optional<Survivor> findByName(String name);

  Survivor findThrowNotFound(String name) throws Exception;

  List<Survivor> listSurvivors();

  Survivor save(Survivor survivorUpdated);
}
