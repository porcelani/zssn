package com.zssn.core.service;

import com.zssn.core.exception.TradeUnrespectPriceTableException;
import com.zssn.core.domain.Item;
import com.zssn.core.domain.Survivor;
import com.zssn.core.domain.Trade;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public final class TradeService {


  private final SurvivorService survivorService;

  public TradeService(SurvivorService survivorService) {
    this.survivorService = survivorService;
  }


  public void tradeResources(final Trade trade) throws Exception {
    Survivor survivorA = trade.getSurvivorA();
    Map<Item, Integer> resourcesA = survivorA.getResources();
    Survivor survivorB = trade.getSurvivorB();
    Map<Item, Integer> resourcesB = survivorB.getResources();

    Survivor survivorASaved = survivorService.findThrowNotFound(survivorA.getName());
    Map<Item, Integer> resourcesASaved = survivorASaved.getResources();
    Survivor survivorBSaved = survivorService.findThrowNotFound(survivorB.getName());
    Map<Item, Integer> resourcesBSaved = survivorBSaved.getResources();


    removeResources(resourcesASaved, resourcesB, resourcesA);
    removeResources(resourcesBSaved, resourcesA, resourcesB);


    survivorService.updateResources(survivorASaved.getName(), resourcesASaved);
    survivorService.updateResources(survivorBSaved.getName(), resourcesBSaved);
  }



  private void removeResources(Map<Item, Integer> resourcesSaved, Map<Item, Integer> resourcesToIncludeValues, Map<Item, Integer> resourcesToExcluideValues) {
    List<Item> items = Arrays.asList(Item.values());
    items.forEach(item -> {
      int value = resourcesSaved.get(item) + resourcesToIncludeValues.get(item) - resourcesToExcluideValues.get(item);
      if (thereAreResourcesToExchange(value)) {
        throw new TradeUnrespectPriceTableException();
      }
      resourcesSaved.put(item, value);
    });

  }

  private boolean thereAreResourcesToExchange(int value) {
    return value < 0;
  }

}
