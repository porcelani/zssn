package com.zssn.core.service;

import com.zssn.core.exception.SurvivorInfectedException;
import com.zssn.core.domain.Item;
import com.zssn.core.domain.Survivor;
import com.zssn.core.ports.SurvivorRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public final class SurvivorService {
  private static final int QUANTITY_OF_REPORTS_TO_CONSIDER_SURVIVOR_A_ZOMBIE = 3;
  private static final boolean NO_INFECTION = false;

  private final SurvivorRepository repository;

  public SurvivorService(SurvivorRepository repository) {
    this.repository = repository;
  }


  public Survivor create(final Survivor survivor) {
    survivor.setName(survivor.getName().toLowerCase());
    survivor.setInfected(NO_INFECTION);
    return repository.create(survivor);
  }

  public Optional<Survivor> find(final String name) {
    Optional<Survivor> survivor = repository.findByName(name);
    if (survivor.isPresent() && !survivor.get().isInfected()) {
      return survivor;
    }
    return Optional.empty();
  }

  public Survivor findThrowNotFound(final String name) throws Exception {
    Survivor survivor = repository.findThrowNotFound(name);
    if (survivor.isInfected()) {
      throw new SurvivorInfectedException();
    }
    return survivor;
  }

  public List<Survivor> listSurvivors() {
    return repository.listSurvivors().stream()
      .filter(survivor -> !survivor.isInfected())
      .collect(Collectors.toList());
  }

  public List<Survivor> listAll() {
    return new ArrayList<>(repository.listSurvivors());
  }

  public void updateLocation(Survivor survivor) {
    find(survivor.getName())
      .ifPresent(
        survivorUpdated -> {
          survivorUpdated.setLocation(survivor.getLocation());
          repository.save(survivorUpdated);
        });
  }

  public void addReportThatSurvivorWasInfected(Survivor savedSurvivor) {
    Optional<Survivor> survivor = find(savedSurvivor.getName());
    survivor
      .ifPresent(
        survivorUpdated -> {
          survivorUpdated.flagTheSurvivorAsInfected(savedSurvivor.whoFlagTheSurvivorAsInfected());
          verifyInfection(survivorUpdated);
          repository.save(survivorUpdated);
        });
  }

  public void updateResources(String name, Map<Item, Integer> resourcesASaved) {
    Optional<Survivor> survivor = find(name);
    survivor
      .ifPresent(
        survivorUpdated -> {
          survivorUpdated.setResources(resourcesASaved);

          repository.save(survivorUpdated);
        });
  }



  private void verifyInfection(Survivor survivorUpdated) {
    if (survivorUpdated.whoFlagTheSurvivorAsInfected().size() >= QUANTITY_OF_REPORTS_TO_CONSIDER_SURVIVOR_A_ZOMBIE) {
      survivorUpdated.setInfected(true);
    }
  }
}
