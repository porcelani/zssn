package com.zssn.core.service;

import com.zssn.core.domain.Item;
import com.zssn.core.domain.Reports;
import com.zssn.core.domain.Survivor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

@Service
public final class ReportService {


  private final SurvivorService survivorService;

  public ReportService(SurvivorService survivorService) {
    this.survivorService = survivorService;
  }


  public Reports getReports() {
    List<Survivor> records = survivorService.listAll();

    Reports reports = new Reports();
    reports.setPercentageOfInfectedSurvivors(getPercentageOfInfectedSurvivors(records));
    reports.setPercentageOfNonInfectedSurvivors(getPercentageOfNonInfectedSurvivors(records));
    reports.setPointsOfResourcesLost(getPointsOfResourcesLost(records));
    reports.setAverageOfResources(getAverageOfResources(records));
    return reports;

  }


  private Map<Item, Integer> getAverageOfResources(List<Survivor> records) {
    Long nonInfectedSurvivors = getNonInfectedSurvivors(records);
    Map<Item, Integer> averageOfResources = createResourcesMap();
    sumAllResources(records, averageOfResources);
    calculateResourcesBySurvivor(nonInfectedSurvivors, averageOfResources);

    return averageOfResources;
  }

  private void calculateResourcesBySurvivor(Long nonInfectedSurvivors, Map<Item, Integer> averageOfResources) {
    averageOfResources.keySet().forEach(item -> {
      averageOfResources.put(item, (int) (averageOfResources.get(item) / nonInfectedSurvivors));
    });
  }

  private void sumAllResources(List<Survivor> records, Map<Item, Integer> averageOfResources) {
    records.stream().filter(survivor1 -> !survivor1.isInfected()).forEach(survivor -> {
      Map<Item, Integer> resources = survivor.getResources();
      averageOfResources.keySet().forEach(item -> averageOfResources.put(item, averageOfResources.get(item) + resources.get(item)));
    });
  }

  private Integer getPointsOfResourcesLost(List<Survivor> records) {
    return records.stream().filter(Survivor::isInfected).mapToInt(Survivor::resourcesPoints).sum();
  }

  private float getPercentageOfNonInfectedSurvivors(List<Survivor> records) {
    Integer allRecords = records.size();
    Long nonInfectedSurvivors = getNonInfectedSurvivors(records);
    return (float) (nonInfectedSurvivors / allRecords * 100);
  }

  private long getNonInfectedSurvivors(List<Survivor> records) {
    return records.stream().filter(survivor -> !survivor.isInfected()).count();
  }

  private float getPercentageOfInfectedSurvivors(List<Survivor> records) {
    Integer allRecords = records.size();
    Long infectedSurvivors = records.stream().filter(Survivor::isInfected).count();
    return (float) (infectedSurvivors / allRecords * 100);
  }

  private Map<Item, Integer> createResourcesMap() {
    Map<Item, Integer> averageOfResources = new HashMap<>();

    asList(Item.values())
      .forEach(item ->
        averageOfResources.put(item, 0));
    return averageOfResources;
  }

}
