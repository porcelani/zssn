package com.zssn.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "survivor is infected, their inventory items become inaccessible (they cannot trade with others)")
public class SurvivorInfectedException extends RuntimeException {
}
