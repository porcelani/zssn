package com.zssn.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "survivor not found")
public class SurvivorNotFoundException extends RuntimeException {
}
