package com.zssn.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Survivors can not trade items among themselves without respect the price table")
public class TradeUnrespectPriceTableException extends RuntimeException {
}
