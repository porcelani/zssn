# ZSSN (Zombie Survival Social Network)

## Required
- Java 8
- Maven

## How Build
```
mvn clean install
```

## How Run
```
java -jar target/*.jar
```

## API Documentation:
### Service Status
```
curl -X GET "localhost:8081/actuator/health"

---

{"status":"UP"}
```

### Register Survivor
```
curl -X POST -H "Content-Type: application/json" "localhost:8080/survivors" -d '
{  
   "name":"Porcelani",
   "age":32,
   "gender":"MALE",
   "location":{  
      "latitude":-23.4059175,
      "longitude":-52.0380783
   },
   "resources":{
      "WATER": 1,
      "FOOD": 2,
      "AMMUNITION": 4
   }
}'

---

{
    "name": "porcelani",
    "name":"Porcelani",
    "age":32,
    "gender":"MALE",
    "infected" : false,   
    ...
}

```

```
curl -X POST -H "Content-Type: application/json" "localhost:8080/survivors" -d '
{  
   "name":"Silva",
   "age":33,
   "gender":"FEMALE",
   "location":{  
      "latitude":-23.4059175,
      "longitude":-52.0380783
   },
   "resources":{
      "MEDICATION": 1,
      "AMMUNITION": 4
   }
}'

---

{
    "name": "silva",
    "age":33,
    "gender":"FEMALE",
    "infected" : false,   
    ...
}

```




### List Survivors
```
curl -X GET -H "Content-Type: application/json" "localhost:8080/survivors" 

---

[
  {
    "name": "porcelani",
    ...
  },
  ...    
]
```


### List Survivor
```
curl -X GET -H "Content-Type: application/json" "localhost:8080/survivors/porcelani" 

---

{
   "name": "porcelani",
   ...
}

```


### Update Survivor Location
```
curl -X PUT -H "Content-Type: application/json" "localhost:8080/survivors/porcelani" -d '
{
  "location": {
    "latitude" : -22.5305866,
    "longitude" : -50.8792296
   }  
}'

---

200 OK

```


### Flag Survivor as Infected
```
curl -X POST -H "Content-Type: application/json" "localhost:8080/survivors/porcelani" -d '
{
  "who_flag_the_survivor_as_infected": ["silva"]
}'

---

200 OK

```


### Trade items 
```
curl -X POST -H "Content-Type: application/json"  "localhost:8080/trades" -d '
{ 
  "survivor_a":{
    "name":"porcelani",
    "resources":{
      "WATER": 1
    }
  },
  "survivor_b":{  
    "name":"silva",
    "resources":{
      "AMMUNITION":4
    }
  }
}'

---

202 Accepted

```


### Trade items 
```
curl -X GET -H "Content-Type: application/json" "localhost:8080/reports"

---

{
  "percentage_of_infected_survivors": 0,
  "percentage_of_non_infected_survivors": 100,
  "average_of_resources": { 
    "WATER": 0,
    "FOOD": 1,
    "MEDICATION": 0,
    "AMMUNITION": 4
  },
  "points_of_resources_lost": 0
}


```
