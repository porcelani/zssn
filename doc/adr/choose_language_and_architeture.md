# Java 8

## Decision

I opted for Java in version 8 because it was a language and a version that I work for a long time and I imagine that I will have access to everything on the ecosystem of language.
I will use some tools that are not native to language (let us say that I am a lucky guy and computer that I found was complete with everything).

So, I opted for the following tools;
- Maven: dependency manager;
- Spring: Stand-alone application;
- Junit: a testing framework for the Java programming language;
- InMemoryDatabase: To use a real database just need to implement the  * .Repository.java;



## Consequences

My initial objective was to carry out the entire implementation with "Vanilla Java" but between compiling (javac ...),
package (jar ...) and run (java -jar ...), besides being obliged to raise a server with class HttpServer, or client com or HttpURLConnection,
Manipulating everything with BufferedReader, Marshaller, Unmarshaller and without an infrastructure for you testes, I believe I already would have zombie food (imagine you have to implement it in writing the persist in a file).
Probably if I had limited resources in the ecosystem I would have opted for another approach and probably another language.
